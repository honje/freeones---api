# An unofficial Freeones.la Node.js Api

Table of Content

- [Api](#api)
  - [getBabe](#getbabe)
  - [getBabes](#getbabes)
  - [getAlbum](#getalbum)

## Usage

```typescript
import freeonesApi from 'freeonesapi'
```

## Api

### getBabe

Gets Information from a Single Babe

Option | Type | Default Value | Description |
:---:|:---:|:---:|:---:|
| name | String | - | Name of the Porn Star |
| options | [BabeOptions](https://gitlab.com/honje/freeones---api/tree/master/src/models/BabeOptions.ts) | see Example | Options too alter the output |

```typescript
freeonesApi.getBabe(
  'Ava Addams',
  {
    InfoOptions: {
      request: true
    },
    PhotoOptions: {
      request: true,
      importAmmount: 12,
    },
    LinkOptions: {
      request: true,
      importAmmount: 12,
    }
  }
  ).then(res => {
  ...
})
```

Returns [Babe](https://gitlab.com/honje/freeones---api/tree/master/src/models/Babe.ts)

### getBabes

Gets Information from the Babes page

#### Parameter

##### filter

Filterschema see on [Gitlab](https://gitlab.com/honje/freeones---api/blob/master/src/models/BabeFilter.ts)

##### options

Optionsschema see on [Gitlab](https://gitlab.com/honje/freeones---api/blob/master/src/models/GetBabesOptions.ts)

- `reRequest`: Boolean<br />sets if the function is allowed too make more then one request
- `returnLength`: Number<br />if `reRequest` is true the Function is requesting new Pages as long as the return Array has a length greater or equal to this value.
- `customFilter`: async Function<br />The `customFIlter`-Function is used on every babe and is passed [`BabeSimple`](https://gitlab.com/honje/freeones---api/blob/master/src/models/BabeSimple.ts) Element and must respond with a `Promise<boolean>` type. If the Fuction returns true it will save the current Babe if False it will kick the item out of the response.

#### Example

Option | Type | Default Value | Description |
:---:|:---:|:---:|:---:|
| filter | [BabeFilter](https://gitlab.com/honje/freeones---api/tree/master/src/models/BabeFilter.ts) | see Example | Object that contains Filter too alter the Return |
| options | [GetBabesOptions](https://gitlab.com/honje/freeones---api/tree/master/src/models/GetBabesOptions.ts) | see Example | Object that contains Options for the Request |

```typescript
freeonesApi.getBabes({
  display: 12,
  sortBy: sortByFilter.lat,
  keyword: '',
  ordering: orderingFilter.asc
}, {
  reRequest: false,
  returnLength: 12,
  customFilter: async () => true
}).then(res => {
  ...
})
```

Returns [BabeSimple[]](https://gitlab.com/honje/freeones---api/tree/master/src/models/BabeSimple.ts)

### getAlbum

Gets Photos from the Album page

Option | Type | Default Value | Description |
:---:|:---:|:---:|:---:|
| link | String | - | Link to the Album |

```typescript
freeonesApi.getAlbum('ava-addams/photos/ava-addams-loves-to-get-nude-and-show-big-melons').then(res => {
  ...
})
```

Option | Type | Default Value | Description |
:---:|:---:|:---:|:---:|
| babeName | String | - | Name of the Babe |
| albumName | String | - | Name of the Album |

```typescript
freeonesApi.getAlbum('Ava Addams', 'Ava Addams loves to get nude and show big Melons').then(res => {
  ...
})
```

Returns [Album[]](https://gitlab.com/honje/freeones---api/tree/master/src/models/Album.ts)