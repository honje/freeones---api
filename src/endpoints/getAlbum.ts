import {defaultUrl} from '../config'
import {
	fetchPage, toArray
} from '../utils/request'
import {Album} from '..'

export async function getAlbum(link: string): Promise<Album[]>
export async function getAlbum(babeName: string,albumName?: string): Promise<Album[]>
export async function getAlbum(babeName: string,albumName?: string): Promise<Album[]> {
	let url
	if(albumName === undefined) url = `${defaultUrl}/${babeName}`
	else url = `${defaultUrl}/${babeName.replace(' ', '-').toLowerCase()}/photos/${albumName.replace(' ', '-').toLowerCase()}`
  
	const $ = await fetchPage(url)

	return toArray($('figure')).map((value) => {
		const src = value.children('a').attr('href')
		const res = value.children('a').attr('data-size')

		return {
			src,
			res
		}
	})
}