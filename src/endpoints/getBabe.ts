import {
	defaultUrl,
	defaultBabePhotoUrl,
	defaultBabeLinkUrl,
	defaultBabeProfileUrl
} from '../config'
import {
	fetchPage,
	toArray
} from '../utils/request'
import {Babe} from '..'
import {BabeOptions} from '../models/BabeOptions'
import {BabeLink} from '../models/BabeLink'
import {BabePhoto} from '../models/BabePhoto'
import {BabeAlbum} from '../models/BabeAlbum'
import {BabeInfos} from '../models/BabeInfos'
import {cleanString} from '../utils/string'

const babeOptionDefault: BabeOptions = {
	InfoOptions: {request: true},
	PhotoOptions: {
		request: true,
		importAmount: 12,
	},
	LinkOptions: {
		request: true,
		importAmount: 12,
	}
}

function getProfileInfo($profile: CheerioStatic): BabeInfos {
	// Reading out of the Header
	const headerContent = $profile('main section header')
	const headerLeft = headerContent.find('.header-content > div > div:first-of-type')
	const headerRight = headerContent.find('.header-content > div > div:last-of-type')
	const ranking = headerRight.find('> div:first-of-type div#vue-subject-rank-chart')

	const name = headerLeft
		.find('h2 > span')
		.text()

	const id = name.replace(/ /g, '-').toLowerCase()

	const views = parseInt(headerRight
		.find('> div:last-of-type span')
		.text(), 10)

	const ratingHtml = headerLeft
		.find('> div:first-of-type > div > div:first-of-type')
		.html()
	const rating = ratingHtml ? parseFloat(ratingHtml.split('"')[3]) : 0

	let rankingRet
	if(ranking[0]) {
		rankingRet = {
			place: parseInt(ranking[0].children[1].attribs[':current-rank'], 10),
			change: parseInt(ranking[0].children[1].attribs[':rank-delta'], 10)
		}
	}
	else {rankingRet = {
		place: undefined , change: undefined
	}}

	// Reading out of the Profile Page
	const profileContent = $profile('main > div > div:first-of-type section')
	const infoContent = profileContent.find('> div:nth-of-type(2)')

	const isAgeThere = infoContent.find('> div:first-of-type > div > p:nth-of-type(2) a')[0]

	const birthDate = infoContent.find('> div:first-of-type > div > p:first-of-type a')[0] && isAgeThere ?
		String(infoContent
			.find('> div:first-of-type > div > p:first-of-type a')
			.attr('href'))
			.split('=')[1] :
		undefined

	const age = infoContent.find('> div:first-of-type > div > p:nth-of-type(2) a')[0] ?
		parseInt(String(infoContent
			.find('> div:first-of-type > div > p:nth-of-type(2) a')
			.attr('href'))
			.split('=')[1], 10) :
		undefined

	const country = infoContent.find('> div:first-of-type > div > p:last-of-type a:last-of-type')[0] ?
		String(infoContent
			.find('> div:first-of-type > div > p:last-of-type a:last-of-type')
			.attr('href'))
			.split('=')[1] :
		undefined

	const birthPlace = infoContent.find('> div:first-of-type > div > p:last-of-type a:nth-of-type(2)')[0] && isAgeThere ?
		String(infoContent
			.find('> div:first-of-type > div > p:last-of-type a:nth-of-type(2)')
			.attr('href'))
			.split('=')[1] :
		undefined

	const ethnicity = infoContent
		.find('> div:last-of-type > div:first-of-type > div:nth-of-type(2) span')
		.text()

	const website = infoContent.find('> div:last-of-type > div:first-of-type > div:last-of-type a')[0] ?
		infoContent
			.find('> div:last-of-type > div:first-of-type > div:last-of-type a')
			.attr('href') :
		undefined

	const astrologicalSign = infoContent
		.find('> div:last-of-type > div:last-of-type > div:first-of-type span')
		.text()
		.split(' ')[0]

	const socialMedia: any[] = []
	infoContent.find('> div:last-of-type > div:last-of-type > div:last-of-type a')[0] ?
		infoContent
			.find('> div:last-of-type > div:last-of-type > div:last-of-type a')
			.map((_: number, element: CheerioElement) => {
				const el = $profile(element)

				const title = el.attr('title')

				const link = el.attr('href')

				const platformdomain = String(el.attr('href')).split('/')[2].split('.')
				let platform
				platformdomain[2] ? platform = platformdomain[1] : platform = platformdomain[0]
				platform = platform.charAt(0).toUpperCase() + platform.slice(1)

				socialMedia[socialMedia.length] = {
					platform,
					title,
					link,
				}
				return true
			}) :
		undefined

	// Reading from Appearance Sector on Profile Page
	const appearanceContent = profileContent.find('> div:last-of-type')

	const eyeColor = appearanceContent.find('> div:first-of-type a')[0] ?
		String(appearanceContent
			.find('> div:first-of-type a')
			.attr('href'))
			.split('=')[1] :
		undefined

	const hairColor = appearanceContent.find('> div:nth-of-type(2) a')[0] ?
		String(appearanceContent
			.find('> div:nth-of-type(2) a')
			.attr('href'))
			.split('=')[1] :
		undefined

	const height = appearanceContent.find('> div:nth-of-type(3) a')[0] ?
		parseInt(String(appearanceContent
			.find('> div:nth-of-type(3) a')
			.attr('href'))
			.split('=')[1]
			.split('%2C')[0], 10) :
		undefined

	const weight = appearanceContent.find('> div:nth-of-type(4) a')[0] ?
		parseInt(String(appearanceContent
			.find('> div:nth-of-type(4) a')
			.attr('href'))
			.split('=')[1]
			.split('%2C')[0], 10) :
		undefined

	const braSize = appearanceContent.find('> div:nth-of-type(5) a:first-of-type')[0] ?
		String(appearanceContent
			.find('> div:nth-of-type(5) a')
			.attr('href'))
			.split('=')[1] :
		undefined

	const fakeBoobs = appearanceContent.find('> div:nth-of-type(6) a:first-of-type')[0] ?
		Boolean(String(appearanceContent
			.find('> div:nth-of-type(6) a')
			.attr('href'))
			.split('=')[1]) :
		undefined

	const shoeSize = appearanceContent.find('> div:nth-of-type(7) a:first-of-type')[0] ?
		parseInt(String(appearanceContent
			.find('> div:nth-of-type(7) a')
			.attr('href'))
			.split('=')[1]
			.split('%2C')[0], 10) :
		undefined

	const tattoos = appearanceContent.find('> div:nth-of-type(8) div p')[0] ?
		appearanceContent
			.find('> div:nth-of-type(8) div p')
			.text()
			.split(';') :
		undefined

	const piercings = appearanceContent.find('> div:nth-of-type(9) div p')[0] ?
		appearanceContent
			.find('> div:nth-of-type(9) div p')
			.text()
			.split(';') :
		undefined

	const additional = appearanceContent.find('> div:nth-of-type(10) div p')[0] ?
		appearanceContent
			.find('> div:nth-of-type(10) div p')
			.text()
			.replace(/\n/g, '')
			.replace(/ {2}/g, '') :
		undefined

	return {
		id,
		name,
		age,
		birthDate,
		birthPlace,
		website,
		views,
		country,
		socialMedia,
		astrologicalSign,
		ethnicity,
		rating: rating,
		ranking: rankingRet,
		appearance: {
			eyeColor,
			hairColor,
			height,
			weight,
			braSize,
			fakeBoobs,
			shoeSize,
			tattoos,
			piercings,
			additional
		}
	}
}

function getPhotoInfo($photo: CheerioStatic): BabePhoto {
	const albumsArr: BabeAlbum[] = []

	const photoContent = $photo('main > div > div:first-of-type')
	const albums = photoContent.find('div:first-of-type > a')

	toArray(albums).map((value: Cheerio) => {
		const el = $photo(value)

		const link = defaultUrl + el.attr('href')

		const img = String(el.find('> img').attr('src'))

		const imgCount = parseInt(el.find('> div p span').text(), 10)

		const newAlbum = !!el.find('> div > div > span')[0]

		albumsArr[albumsArr.length] = {
			link,
			img,
			imgCount,
			new: newAlbum
		}

		return true
	})

	return {
		albumCount: albumsArr.length,
		albums: albumsArr
	}
}

function getLinkInfo($link: CheerioStatic): BabeLink[] {
	const returnArr: BabeLink[] = []
	const linkContent = $link('main > div > div:first-of-type > div:first-of-type > div')
	const firstLinkClean = cleanString($link(linkContent['0']).text())

	if(firstLinkClean !== 'Therearenomatchinglinks') {
		linkContent.map((_: number, value: CheerioElement) => {
			const el = $link(value)

			const link = defaultUrl + el.find('> a > div > p:last-of-type').text()

			const pinned = !!el.find('svg')[0]

			const title = el.find('a div div p').text().replace(/\n/g, '').replace(/ {2}/g, '')

			const img = String(el.find('> a > img').attr('src'))

			returnArr[returnArr.length] = {
				link,
				pinned,
				title,
				img,
			}
			return true
		})
	}

	return returnArr
}

export async function getBabe(name: string,options?: BabeOptions): Promise<Babe> {
	try {
		options = {
			...babeOptionDefault, ...options
		}
		const babeName = name.replace(/ /g, '-').toLowerCase()

		// Url for the Img-Gallery's
		const photoUrl = `${defaultUrl}/${babeName}/${defaultBabePhotoUrl}?
      v=rows
      &l=${options.PhotoOptions ? options.PhotoOptions.importAmount : 12}`
			.replace(/\n/g, '')
			.replace(/ {2}/g, '')
		// Url for the Links
		const linkUrl = `${defaultUrl}/${babeName}/${defaultBabeLinkUrl}?
      v=rows
      &l=${babeOptionDefault.LinkOptions ? babeOptionDefault.LinkOptions.importAmount : 12}`
			.replace(/\n/g, '')
			.replace(/ {2}/g, '')
		// Url for the Profile
		const profileUrl = `${defaultUrl}/${babeName}/${defaultBabeProfileUrl}`


		const $photo = options.PhotoOptions && options.PhotoOptions.request ? await fetchPage(photoUrl) : false
		const $link = options.LinkOptions && options.LinkOptions.request ? await fetchPage(linkUrl) : false
		const $profile = options.InfoOptions && options.InfoOptions.request ? await fetchPage(profileUrl) : false


		let photoContent: BabePhoto | undefined = undefined
		if(options.PhotoOptions && options.PhotoOptions.request && $photo) photoContent = getPhotoInfo($photo)

		let linksContent: BabeLink[] | undefined = undefined
		if(options.LinkOptions && options.LinkOptions.request && $link) linksContent = getLinkInfo($link)

		let infoContent: BabeInfos | undefined = undefined
		if(options.InfoOptions && options.InfoOptions.request && $profile) infoContent = getProfileInfo($profile)

		return {
			info: infoContent,
			photos: photoContent,
			links: linksContent
		}
	}
	catch(error) {
		throw new Error(error)
	}
}
