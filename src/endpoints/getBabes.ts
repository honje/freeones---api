import {
	debugMode, defaultUrl
} from '../config'
import {
	fetchPage, toArray
} from '../utils/request'
import {BabeFilter} from '..'
import {BabeSimple} from '..'
import {orderingFilter} from '../enums/orderingFilter'
import {sortByFilter} from '../enums/sortByFilter'
import {GetBabesOptions} from '../models/GetBabesOptions'


const filterDefault: BabeFilter = {
	display: 12,
	keyword: '',
	ordering: orderingFilter.asc,
	sortBy: sortByFilter.latest,
	page: 1
}

const optionsDefault: GetBabesOptions = {
	reRequest: false,
	returnLength: 12,
	customFilter: async () => true
}

export async function getBabes(filter?: BabeFilter, options?: GetBabesOptions): Promise<BabeSimple[]> {
	const filterOb = {
		...filterDefault, ...filter
	}
	const optionsOb = {
		...optionsDefault, ...options
	}


	const url = `${defaultUrl}/babes
    ?v=rows
    &q=${filterOb.keyword}
    &l=${filterOb.display}
    &s=${filterOb.sortBy}
    &o=${filterOb.ordering}
    &p=${filterOb.page}
    ${filterOb.uploadDate ? `&d[createdAt]=${filterOb.uploadDate}` : ''}
    ${filterOb.careerStatus ? `&f[careerStatus]=${filterOb.careerStatus}` : ''}
    ${filterOb.age ? `&d[dateOfBirth]=${filterOb.age}` : ''}
    ${filterOb.country ? `&f[country]=${filterOb.country}` : ''}
    ${filterOb.placeOfBirth ? `&f[placeOfBirth]=${filterOb.placeOfBirth.join(',')}` : ''}
    ${filterOb.eyeColor ? `&f[eyeColor]=${filterOb.eyeColor.join(',')}` : ''}
    ${filterOb.hairColor ? `&f[hairColor]=${filterOb.hairColor.join(',')}` : ''}
    ${filterOb.height ? `&r[height]=${filterOb.height.join(',')}` : ''}
    ${filterOb.weight ? `&r[weight]=${filterOb.weight.join(',')}` : ''}
    ${filterOb.fakeBoobs ? `&r[fakeBoobs]=${filterOb.fakeBoobs ? 1 : 0}` : ''}
    ${filterOb.shoeSize ? `&r[shoeSize]=${filterOb.shoeSize.join(',')}` : ''}
    ${filterOb.ethnicity ? `&f[ethnicity]=${filterOb.ethnicity.join(',')}` : ''}
    ${filterOb.profession ? `&f[professions]=${filterOb.profession.join(',')}` : ''}
    ${filterOb.bra ? `&f[bra]=${filterOb.bra.join(',')}` : ''}
    ${filterOb.zodiac ? `&f[astrologicalSign]=${filterOb.zodiac}` : ''}`
		.replace(/\n/g, '')
		.replace(/ {2}/g, '')

	const $ = await fetchPage(url)
	const mainContent = toArray($('main > div > div > div:first-of-type > a'))
	if(!mainContent[0]) throw new Error('no Content Found on the Page')

	const rmArr: number[] = []
	const newReturnValue: Promise<BabeSimple>[] = mainContent
		.map((value: Cheerio) => {
			const el = $(value)

			const id = String(el
				.attr('href'))
				.slice(1)
			const name = String(el
				.attr('href'))
				.slice(1)
				.split('-')
				.map(value => value.charAt(0).toUpperCase() + value.slice(1))
				.join(' ')
			const nation = el.find('> div > p:first-of-type i').attr('title')
			const link = `${defaultUrl}${el.attr('href')}`
			const img = el.find('> img').attr('src')
			const followers = parseInt(el
				.find('> div > p:last-of-type')
				.text()
				.replace(' followers', ''), 10)
			const videoCount = parseInt(el.find('div div p span:nth-of-type(1)').text(), 10)
			const photoCount = parseInt(el.find('div div p span:nth-of-type(2)').text(), 10)
			const linkCount = parseInt(el.find('div div p span:nth-of-type(3)').text(), 10)

			return {
				id,
				name,
				link,
				nation,
				profileImg: img,
				followers,
				links: linkCount,
				videos: videoCount,
				photos: photoCount
			}
		})
		.map(async (el: BabeSimple, index: number) => {
			let rem = false
			let val

			if(filterOb.hasPhotos && el.photos === 0) rem = true

			if(optionsOb.customFilter)val = await optionsOb.customFilter(el)
			if(!val) rem = true

			if(rem) rmArr[rmArr.length] = index
			return el
		})
	let returnValue: BabeSimple[] = await Promise.all(newReturnValue)
	returnValue = returnValue.filter((_, index: number) => !rmArr.includes(index))

	if(optionsOb.returnLength && optionsOb.reRequest) {
		while(returnValue.length < optionsOb.returnLength) {
			try {
				const inWhileOptions = {reRequest: false}

				filterOb.page = filterOb.page ? filterOb.page + 1 : 1

				const newBabesValue = await getBabes(filterOb, {
					...optionsOb, ...inWhileOptions
				})
				newBabesValue.map((el: BabeSimple) => {
					returnValue[returnValue.length] = el
				})
				returnValue = returnValue.concat(newBabesValue)
				debugMode ? console.debug(`Found ${returnValue.length} from ${optionsOb.returnLength}`) : null
			}
			catch (e) {
				break
			}
		}
		returnValue = await Promise.all(returnValue)
	}

	return returnValue
}
