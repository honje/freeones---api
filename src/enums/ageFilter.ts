export enum ageFilter {
  age18 = 'age_18_25',
  age18to25 = 'age_18_25',
  age26 = 'age_26_35',
  age26to35 = 'age_26_35',
  age36 = 'age_36_50',
  age36to50 = 'age_36_50',
  age50 = 'age_51_199',
  age51to199 = 'age_51_199',
}