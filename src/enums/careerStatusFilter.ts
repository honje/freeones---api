export enum careerStatusFilter {
  unknown = 'unknown',
  active = 'active',
  retired = 'retired',
  deceased = 'deceased',
}