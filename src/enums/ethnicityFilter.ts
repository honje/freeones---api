export enum ethnicityFilter {
  caucasian = 'caucasian,Caucasian',
  asian = 'asian',
  latin = 'latin',
  black = 'black',
  white = 'White',
  whiteCau = 'White%2FCaucasian',
  whiteCaucasian = 'White%2FCaucasian',
  hispanic = 'Hispanic',
  european = 'European',
  other = 'Other',
}