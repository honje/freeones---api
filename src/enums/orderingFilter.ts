export enum orderingFilter {
  asc = 'asc',
  ascending = 'asc',
  desc = 'desc',
  descending = 'desc',
}