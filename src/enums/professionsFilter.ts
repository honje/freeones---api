export enum professionsFilter {
  sportswomen = 'sportswomen',
  supermodels = 'supermodels',
  tvHosts = 'tv_hosts',
  actresses = 'actresses',
  musicians = 'musicians',
  centerfolds = 'centerfolds',
  camGirl = 'cam_girl',
  pornStars = 'porn_stars',
  adultModels = 'adult_models',
}