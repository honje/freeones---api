export enum uploadDateFilter {
  beginning = 'since_beginning',
  sinceBeginning = 'since_beginning',
  yesterday = 'since_yesterday',
  sinceYesterday = 'since_yesterday',
  lastWeek = 'last_week',
  last30days = 'last_30_days',
  last3months = 'last_3_months',
}