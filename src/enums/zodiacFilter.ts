export enum zodiacFilter {
  capricorn = 'capricorn',
  sagittarius = 'sagittarius',
  leo = 'leo',
  taurus = 'taurus',
  vigro = 'vigro',
  cancer = 'cancer',
  aries = 'aries',
  libra = 'libra',
  gemini = 'gemini',
  pisces = 'pisces',
  aquarius = 'aquarius',
  scorpio = 'scorpio',
}