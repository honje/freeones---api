import {getBabe} from './endpoints/getBabe'
import {getBabes} from './endpoints/getBabes'
import {getAlbum} from './endpoints/getAlbum'


export class FreeOnesFactory {

  getBabe = getBabe
  getBabes = getBabes
  getAlbum = getAlbum

}

export * from './models/BabeFilter'
export * from './models/Babe'
export * from './models/Album'
export * from './models/BabeSimple'

const freeonesapi = new FreeOnesFactory()

export default freeonesapi
