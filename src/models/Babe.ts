import {BabeInfos} from './BabeInfos'
import {BabePhoto} from './BabePhoto'
import {BabeLink} from './BabeLink'

export interface Babe {
  info: BabeInfos | undefined;
  photos: BabePhoto | undefined;
  links: BabeLink[] | undefined;
}