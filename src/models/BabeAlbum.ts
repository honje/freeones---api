export interface BabeAlbum {
  new: boolean;
  link: string;
  img: string;
  imgCount: number;
}