import {sortByFilter} from '../enums/sortByFilter'
import {uploadDateFilter} from '../enums/uploadDateFilter'
import {careerStatusFilter} from '../enums/careerStatusFilter'
import {ageFilter} from '../enums/ageFilter'
import {ethnicityFilter} from '../enums/ethnicityFilter'
import {professionsFilter} from '../enums/professionsFilter'
import {zodiacFilter} from '../enums/zodiacFilter'
import {orderingFilter} from '../enums/orderingFilter'

export interface BabeFilter {
  display?: number;
  sortBy?: sortByFilter;
  uploadDate?: uploadDateFilter;
  careerStatus?: careerStatusFilter;
  age?: ageFilter;
  country?: string;
  placeOfBirth?: string[];
  eyeColor?: string[];
  hairColor?: string[];
  height?: number[];
  weight?: number[];
  fakeBoobs?: boolean;
  shoeSize?: number[];
  keyword?: string;
  ethnicity?: ethnicityFilter[];
  profession?: professionsFilter[];
  bra?: string[];
  zodiac?: zodiacFilter;
  ordering?: orderingFilter;
  page?: number;
  hasPhotos?: boolean;
}