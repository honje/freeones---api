export interface BabeInfos {
  id: string;
  name: string;
  aliases?: string[];
  age?: number;
  birthDate?: string;
  birthPlace?: string;
  website?: string;
  views: number;
  country?: string | undefined;
  socialMedia?: any[];
  astrologicalSign?: string;
  ethnicity?: string;
  rating: number;
  ranking: {
    place: number | undefined;
    change: number | undefined;
  };
  appearance?: {
    eyeColor?: string;
    hairColor?: string;
    height?: number;
    weight?: number;
    braSize?: string;
    fakeBoobs?: boolean;
    shoeSize?: number;
    tattoos?: string[];
    piercings?: string[];
    additional?: string;
  };
}