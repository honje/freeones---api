export interface BabeLink {
  link: string;
  pinned: boolean;
  title: string;
  img: string;
}