export interface BabeOptions {
  InfoOptions?: {
    request?: boolean;
  };
  PhotoOptions?: {
    request?: boolean;
    importAmount?: number;
  };
  LinkOptions?: {
    request?: boolean;
    importAmount?: number;
  };
}