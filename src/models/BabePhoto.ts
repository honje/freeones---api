import {BabeAlbum} from './BabeAlbum'

export interface BabePhoto {
  albumCount: number;
  albums: BabeAlbum[];
}