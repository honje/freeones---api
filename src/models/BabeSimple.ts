export interface BabeSimple {
  id: string;
  name: string;
  link: string;
  nation: string;
  profileImg: string;
  followers: number;
  videos: number;
  photos: number;
  links: number;
}