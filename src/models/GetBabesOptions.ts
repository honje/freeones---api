import {BabeSimple} from './BabeSimple'

export interface GetBabesOptions {
	reRequest?: boolean;
	returnLength?: number;
	customFilter?: (el: BabeSimple) => Promise<boolean>;
}