import freeonesapi from "../index"

test("Happy Path",async () => {
  const babe = await freeonesapi.getBabe('Ava Addams')

  expect(babe.info).toBeDefined()
  expect(babe.info.id).toBeDefined()
  expect(babe.info.name).toBeDefined()
  expect(babe.info.views).toBeDefined()
  expect(babe.info.views).toBeGreaterThanOrEqual(0)
  expect(babe.info.rating).toBeDefined()
  expect(babe.info.ranking).toBeDefined()
  expect(babe.photos).toBeDefined()
  expect(babe.photos.albumCount).toBeDefined()
  expect(babe.photos.albumCount).toBeGreaterThanOrEqual(0)
  expect(babe.photos.albums.length).toBeGreaterThanOrEqual(0)
  expect(babe.links).toBeDefined()
  expect(babe.links.length).toBeGreaterThanOrEqual(0)
  // Testing if a Link is defined
  if(babe.links.length >= 1) {
    expect(babe.links[0].link).toBeDefined()
    expect(babe.links[0].pinned).toBeDefined()
    expect(babe.links[0].title).toBeDefined()
    expect(babe.links[0].img).toBeDefined()
  }
})
