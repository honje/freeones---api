import cheerio from 'cheerio'
import request from 'request'
import {debugMode} from '../config'

async function loadPage(url: string): Promise<any> {
	debugMode ? console.debug(`Requesting: '${url}'`) : null
	return new Promise<any>(resolve => request(url, (_, __, body) => resolve(body)))
}

export async function fetchPage (
	url: string
): Promise<CheerioStatic> {
	return cheerio.load(await loadPage(url))
}

export function toArray (elements: Cheerio): Cheerio[] {
	return elements.toArray().map(cheerio)
}