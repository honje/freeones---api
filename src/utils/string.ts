/**
 * Function too clean strings from spaces and linebreaks
 *
 * @param {string} dirtyString - String that has spaces or linebreaks
 * @returns {string} String without spaces or linebreaks
 */
export function cleanString(dirtyString: string): string {
	return dirtyString
		.replace(/(\r\n|\n|\r)/gm,'') //removes lineBrakes
		.replace(/ /g, '') //removes spaces
}